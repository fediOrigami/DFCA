
![](screenshots/dfca-screenshot.jpg)


Digital Feudalism Counter Action (DFCA)
---------------------------------------

I could not find this anywhere on the clearnet. Seems to be only available on I2P (Invisible Internet Protocol), so thought I would share it here in the interim.

The best and most user-friendly Big Tech Blocker for Linux that shows a little pop-up message when it blocks.

With simple instructions anyone can block the digital feudalists, otherwise known as C.A.G.E.M.A.F.I.A., Cloudflare, Amazon, Google, E. (Musk), Microsoft, Akamai, Facebook, (not IBM) and Akamai. It only requires common software **already installed** on most linux systems. You might only need to install one popular package called Papirus (for icons).

See **DFCA tar file and cryptographic signature** above.

If you find it as good as I do, **please mirror it**.

FAQ
---

**Do I even need this, as a user of duckduckgo, discord, twitch, patreon, openai, steam, odysee, bitchute, gab, LBRY, locals, medium, minds, deviantart, truthsocial, spotify, soundcloud and/or substack? Surely they are safe, right?**

Sorry you've been dot-conned. They are all C.A.G.E.M.A.F.I.A. but don't dispair, DFCA can help you find alternatives, like fediverse for social, different search engines, and more.

---

**What if I don't use linux?**

Using a proprietary Microsoft, Google or Apple OS means not being able to fend off feudalism. Consider a "dual boot" scenario for experimenting with linux. There are interesting distributions like Gentoo, Parabola, Mageia, Debian, Manjaro, BSD and more. Explore, experiment, have fun in the wide world of linux!

---

**What are "reverse-proxies" and why are they bad?**

Read Jeff Cliff's assessment of Cloudflare, which applies to the big "reverse-proxy services", including Amazon Cloudfront and Akamai Ghost. They basically see all communications, unencrypted. These "services" are often accurately described as a Man-in-the-Middle (MITM) attack as-a-service. Such "services" weaved their way into internet infrastructure, while acting as "protection" when sites are said to be attacked. In many circumstances a site owner is unaware CloudFlare is watching their traffic because Internet Service Providers (ISPs) have been known to outsource "protection" to the above corporate behemoths with little or no forewarning.

---

**How is this different to the "Block Cloudflare MITM Attack (BCMA)" browser add-on?**

The BCMA add-on is a good companion to DFCA. If you have DFCA, you only need the BCMA add-on on Tor Browser and any other browser used for mix-networks like I2P, some may use [Tor Browser for I2P also](http://mckinley2nxomherwpsff5w37zrl6fqetvlfayk2qjnenifxmw5i4wyd.onion/notes/20221109-tor-browser-i2p.xhtml). Also remember, BCMA add-on only warns of CloudFlare, not Amazon, Microsoft Akamai et al. I have found there is a nice symmetry, in that when DFCA is installed BCMA addon works only on the browsers configured to use mix-nets eg. I2P, and DFCA works as a total linux firewall for **everything but** the mix-nets - almost perfect compliments. The only thing missing is BCMA addon not detecting more digital feudalists! 

To use the BCMA add-on without introducing a fingerprint risk, change the action in settings from "Block immediately" to "Only change icon color". The act of instant blocking during a page load is a fingerprint risk. Feel free to tick "Change title and border color" beneath that. As always, use Tor Browser's "Safest Mode" to block JavaScript, and only allow JavaScript from domains you consider safe. No-script addon is your friend here, ensure the no-script button is added to the top bar to provide easy access to the no-script dialog.

Unfortunately in 2022, Mozilla mysteriously removed the latest BCMA add-on from their servers, however because I keep copies of software, you can find BCMA in the 'companion-software' folder above. A more updated version may be available at DeCloudflare (https://gitea.slowb.ro/dCF/deCloudflare). If you are unsure about the XPI file, rename it as ".zip" and unzip to read its contents! Yes, you can do that!!

---

**What further risks are we facing?**

Financial services, like bitcoin-friendly banks and exchanges, online shops and donation websites seem to be forced into using the likes of Cloudflare, Amazon and Akamai with extreme aggressiveness. A reasonable person can deduce from this, that no independent banking, finance or shopping institutions that operate outside of the surveillance of the behemoths is allowed to exist. This is functionally equivalent to a CBDC, it is just done by stealth rather than made explicit.

Taken with other evidence of silent and overt repression, in the case of Canada truckers protests, and general media suppression, something akin to CBDCs are actively being tested on various persons of interest or groups of dissidents and are likely affecting them in ways they might not even think to imagine. "Computer says no"? 

The chilling-effect of this sort of surveillance and repression means we are all impacted.

---

**Thanks for considering doing your part to stop the digital-feudalist takeover.** I'm grateful that DFCA anti-trust software has emerged, even if at this very late stage. Thanks cyberpunks!

Best wishes to all,

Satisified DFCA Participant.

PS. Open question to dev(s): How difficult would it be to make DFCA a fully automated install? I think this is important for most people who shun command line interfaces.